extends KinematicBody2D

var angle = 0.0
const SPEED = 50
var motion = Vector2()

func _ready():
	set_as_toplevel(true)
	pass
	
func _physics_process(delta):
	motion.x = SPEED * cos(deg2rad(angle))
	motion.y = SPEED * sin(deg2rad(angle))
	#	motion.y += ACCELERATION * sin(deg2rad(rotation_deg))
	
#	var collision = move_and_collide(motion * delta)
#	if collision:
#		motion = motion.bounce(collision.normal)
#		print("collision info: " + str(collision))



	move_and_slide(motion) # pixels per second. Unlike in for example move_and_collide(), you should not multiply it with delta — this is done by the method.
#	
#	move_and_collide(motion * delta)
	
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
