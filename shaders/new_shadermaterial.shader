shader_type canvas_item;

uniform float time_factor = 2.0;
uniform vec2 amplitude = vec2(30.0, 5.0);

void vertex(){ // it runs once per vertex
	//VERTEX = VERTEX + sin(TIME) * vec2(21.0, 1.0);
	
//	VERTEX.x += sin(TIME) * 21.0;
//	VERTEX.y += cos(TIME) * 21.0;
	
//	VERTEX.x += sin(TIME + VERTEX.x) * 10.0;
//	VERTEX.y += cos(TIME + VERTEX.y) * 21.0;
	
//	VERTEX.x += sin(TIME + VERTEX.y) * 10.0;
//	VERTEX.y += cos(TIME + VERTEX.x) * 21.0;
	
	VERTEX.x += sin(TIME * time_factor) * amplitude.x;
	VERTEX.y += cos(TIME * time_factor) * amplitude.y;

	
}