extends KinematicBody2D

const UP = Vector2(0,-1)
const GRAVITY = 20
const ACCELERATION = 50
const FRICTION_FLOOR_WEIGHT = 0.2 
const FRICTION_AIR_WEIGHT = 0.01 
const MAX_SPEED = 200
const JUMP_HEIGHT = -500

var motion = Vector2()


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _physics_process(delta):
	motion.y += GRAVITY
	var friction = false
	
	if Input.is_action_pressed("ui_right"):
		
		motion.x = min((motion.x + ACCELERATION), MAX_SPEED)
		$Sprite.animation = "Run"
		$Sprite.flip_h = false
	elif Input.is_action_pressed("ui_left"):
		motion.x = max((motion.x - ACCELERATION), -MAX_SPEED)
		$Sprite.animation = "Run"
		$Sprite.flip_h = true
	else:
		$Sprite.animation = "Idle"
		friction = true
		
	if is_on_floor():
		if Input.is_action_pressed("ui_up"):
			motion.y = JUMP_HEIGHT
		if friction == true:
			motion.x = lerp(motion.x, 0, FRICTION_FLOOR_WEIGHT)
	else:
		if motion.y < 0:
			$Sprite.animation = "Jump"
		else:
			$Sprite.animation = "Fall"
		if friction == true:
			motion.x = lerp(motion.x, 0, FRICTION_AIR_WEIGHT)
	
	motion = move_and_slide(motion, UP)
	pass