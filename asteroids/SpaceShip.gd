extends KinematicBody2D

const ACCELERATION = 2
const ROTATION_ACCELERATION = 5
const MAX_SPEED = 100
var motion = Vector2()
var rotation_deg = 0

const SCREEN_WIDTH = 480
const SCREEN_HEIGHT = 270

func _ready():
	# print(str(sin(deg2rad(rotation_deg))))
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass
	
func _input(event):
	
	if event.is_action_pressed("fire"):
		$BulletSpawn.fire(rotation_deg)
		return
		
		
func _process(delta):
	
	
	#get_node("../DebugLabel")
	
	$"../DebugLabel".text = "angle=" + str(rotation_deg) + "\n" + \
							"sin=" + str(sin(deg2rad(rotation_deg))) + "\n" + \
							"cos=" + str(cos(deg2rad(rotation_deg))) + "\n" + \
							"motion=" + str(motion) + "\n" + \
							"pos=" + str(position) + "\n" + \
							"sin=" + str(sin(deg2rad(rotation_deg)))


func _physics_process(delta):
	
	if Input.is_action_pressed("ui_left"):
		rotation_deg -= ROTATION_ACCELERATION
	if Input.is_action_pressed("ui_right"):
		rotation_deg += ROTATION_ACCELERATION
	rotation = deg2rad(rotation_deg)
	
	if Input.is_action_pressed("ui_up"):
		motion.x += ACCELERATION * cos(deg2rad(rotation_deg))
		motion.y += ACCELERATION * sin(deg2rad(rotation_deg))
	if Input.is_action_pressed("ui_down"):
		motion.x -= ACCELERATION * cos(deg2rad(rotation_deg))
		motion.y -= ACCELERATION * sin(deg2rad(rotation_deg))
#		accel = -ACCELERATION
#
#		if motion.x >= 0:
#			motion.x += min((motion.x + accel), MAX_SPEED) * sin(rotation)
#		if motion.y >= 0:
#			motion.y += min((motion.y + accel), MAX_SPEED) * cos(rotation)
#		if motion.x <= 0:
#			motion.x += max((motion.x - accel), -MAX_SPEED) * sin(rotation)
#		if motion.y <= 0:
#			motion.y += max((motion.y - accel), -MAX_SPEED) * cos(rotation)

	
	move_and_slide(motion)
	
	
	if position.x > SCREEN_WIDTH:
		position.x = 0
	if position.y > SCREEN_HEIGHT:
		position.y = 0
		
	if position.x < 0:
		position.x = SCREEN_WIDTH
	if position.y < 0:
		position.y = SCREEN_HEIGHT
		
		
	if motion.x > MAX_SPEED:
		motion.x = MAX_SPEED
	if motion.y > MAX_SPEED:
		motion.y = MAX_SPEED
	if motion.x < -MAX_SPEED:
		motion.x = -MAX_SPEED
	if motion.y < -MAX_SPEED:
		motion.y = -MAX_SPEED
		
		
	if rotation_deg > 360:
		rotation_deg = -360
	if rotation_deg < -360:
		rotation_deg = 360
