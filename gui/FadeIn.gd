extends ColorRect

signal fade_finished

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
func fade_in():
	$AnimationPlayer.play("fade_in")
	

func _on_AnimationPlayer_animation_finished(anim_name):
	emit_signal("fade_finished")
	pass # replace with function body
