extends Node2D

var bullet = preload("Bullet.tscn")

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func fire(angle):
	var new_bullet = bullet.instance()
	new_bullet.angle = angle
	new_bullet.position = get_parent().position
	#new_bullet.angle = angle
	#get_tree().add_child(new_bullet)
	
	get_parent().get_parent().add_child(new_bullet)