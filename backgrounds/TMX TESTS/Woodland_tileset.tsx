<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Woodland_Group" tilewidth="16" tileheight="16" tilecount="64" columns="8">
 <image source="tiled_autotile_test.png" width="128" height="128"/>
 <terraintypes>
  <terrain name="Agua" tile="9"/>
 </terraintypes>
 <tile id="0" terrain=",,,0"/>
 <tile id="1" terrain=",,0,0"/>
 <tile id="2" terrain=",,0,"/>
 <tile id="4" terrain="0,0,0,"/>
 <tile id="5" terrain="0,0,,0"/>
 <tile id="8" terrain=",0,,0"/>
 <tile id="9" terrain="0,0,0,0"/>
 <tile id="10" terrain="0,,0,"/>
 <tile id="12" terrain="0,,0,0"/>
 <tile id="13" terrain=",0,0,0"/>
 <tile id="16" terrain=",0,,"/>
 <tile id="17" terrain="0,0,,"/>
 <tile id="18" terrain="0,,,"/>
</tileset>
