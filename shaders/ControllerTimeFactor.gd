tool

extends HSlider

signal amplitude_changed(amplitude)

var amplitude = 10.0 setget set_amplitude

func _on_ControllerTimeFactor_value_changed(value):
	self.amplitude = value # the self is for this to run the function set_amplitude(value) 
	
func set_amplitude(value):
	amplitude = value
	$Label.text = label_start_text + " " + str(amplitude)
	emit_signal("amplitude_changed", value)