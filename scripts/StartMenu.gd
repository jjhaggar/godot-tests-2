extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	
	$CenterContainer/VBoxContainer/VBoxContainer/StartButton.grab_focus()
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_StartButton_pressed():
	print("Hi")
	get_tree().change_scene("res://scenes/World.tscn")
	pass # replace with function body


func _on_ExitButton_pressed():
	get_tree().quit()
	pass # replace with function body
